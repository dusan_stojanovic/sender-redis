# Sender-Redis



## Dependencies

Before starting the service, it is necessary to execute the following commands
to download the required dependencies:

```
go get -u github.com/go-redis/redis/v8
```

## Running code

Code can be run with the following command:

```
go run main.go
```
