package redisclient

import "github.com/go-redis/redis/v8"

func InitRedisClient() *redis.Client {
	return redis.NewClient(
		&redis.Options{
			Addr: "localhost:6379",
		},
	)
}
