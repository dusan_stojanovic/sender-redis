package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"sender-redis/redisclient"
)

type MessageDto struct {
	Content string `json:"content,omitempty"`
}

func main() {
	log.Println("Initializing Redis Client")
	redisClient := redisclient.InitRedisClient()

	defer redisClient.Close()

	message := MessageDto{
		Content: "Hello World!!!",
	}
	body, _ := json.Marshal(message)

	ctx := context.Background()
	for i := 0; i < 10; i++ {
		err := redisClient.Publish(ctx, "messages", body).Err()
		if err != nil {
			fmt.Println("Error: ", err)
		}
	}
}
